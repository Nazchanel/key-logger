# Key_Logger
A simple Key Logger. Obviously don't use this to do harm.

There is only one package that you need to install.

That package is `pynput`.

To install this type this into the terminal:

`pip install pynput`

---


Click the `start_logging.bat` to start logging. Click the ***Esc*** key to end the logging at any time.

You can create a shortcut to the batch file and the `log.txt` file so you can access them from your desktop.

The `end_logging.bat` will force end the program in case of the ***Esc*** shortcut not working.

This logging will obviously not work in the User Account Control (UAC) prompt. This prompt will show when there is a request for Administrator access for a application to run.

Please create an issue if there are any problems.