@ECHO
:: Shuts down the hidden python process forcibly. This might kill other Python processes you might have running.
taskkill /IM pythonw.exe /F
PAUSE 